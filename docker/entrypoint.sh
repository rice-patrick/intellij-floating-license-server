if [ ! -z "$FLS_HOSTNAME" ]
then
  /usr/local/fls/bin/license-server.sh configure --jetty.virtualHosts.names="$FLS_HOSTNAME"
  echo "Hostname Set to $FLS_HOSTNAME"; \
fi

echo "Running FLS"
/usr/local/fls/bin/license-server.sh run