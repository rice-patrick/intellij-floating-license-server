# IntelliJ Floating License Server

This project maintains an AMI image for provisioning Intellij's Floating License Server.

In addition to the AMI, this also maintains a Terraform configuration that will provision
an EC2 server that to use that AMI.