# Find a certificate that is issued
data "aws_acm_certificate" "issued" {
  domain   = "${var.subdomain}.${var.domain_name}"
  statuses = ["ISSUED"]
}

data "aws_route53_zone" "master_zone" {
  name = var.domain_name
}

# Create a route 53 zone for the new domain
resource "aws_route53_zone" "zone" {
  name = "${var.subdomain}.${var.domain_name}"
}

# In order for the DNS to resolve, the name servers need to be added to the
# parent DNS record
resource "aws_route53_record" "dns_ns" {
  name    = "${var.subdomain}.${var.domain_name}"
  type    = "NS"
  zone_id = data.aws_route53_zone.master_zone.id
  records = aws_route53_zone.zone.name_servers
  ttl     = 60
}


# Route traffic from our domain name to the load balancer DNS
resource "aws_route53_record" "lb_redirect" {
  name    = "${var.subdomain}.${var.domain_name}"
  type    = "A"
  zone_id = aws_route53_zone.zone.id

  alias {
    evaluate_target_health = true
    name = module.fargate-cluster.load-balancer-dns
    zone_id = module.fargate-cluster.load-balancer-zone-id
  }

}