data "aws_vpc" "my_vpc" {
  filter {
    name = "tag:Use"
    values = [var.usage]
  }
}

data "aws_subnet_ids" "ecs_private_subnets" {
  filter {
    name = "tag:GenericId"
    values = var.container_subnets
  }

  vpc_id = data.aws_vpc.my_vpc.id
}

data "aws_subnet_ids" "public_subnets" {
  filter {
    name = "tag:GenericId"
    values = var.lb_subnets
  }

  vpc_id = data.aws_vpc.my_vpc.id
}

module "fargate-cluster" {
  source = "git::https://gitlab.com/rice-patrick/terraform-modules.git//aws/compute/ecs-fargate-with-efs/"

  cluster-name = "${var.cluster_name}-${var.usage}"
  container-image = "registry.gitlab.com/rice-patrick/intellij-floating-license-server"
  container-tag = "latest"

  ecs_subnets = data.aws_subnet_ids.ecs_private_subnets.ids
  vpc_id = data.aws_vpc.my_vpc.id

  container-startup-time = 180
  deployment-max-percent = 200
  deployment-min-percent = 100

  lb-health-check-url = "/health"
  lb-subnets = data.aws_subnet_ids.public_subnets.ids
  lb-listener-port = 443
  lb-ssl-certificate-arn = data.aws_acm_certificate.issued.arn
  lb-internal = var.lb_visibility
  lb-allowed-ips = var.allowed_ips

  # Allow the FLS to run completely on spot instances
  fargate-spot-percentage = 100
  base-on-demand-container-count = 0

  # We're using a domain name now, so ignore use that instead of LB DNS
  container-environment = [
    {name = "FLS_HOSTNAME", value = "${var.subdomain}.${var.domain_name},${module.fargate-cluster.load-balancer-dns}"}
  ]

}

resource "aws_security_group_rule" "lb_external_ip_rule" {
  protocol = "TCP"
  security_group_id = module.fargate-cluster.load-balancer-security-group-id
  type = "ingress"

  from_port = 443
  to_port   = 443
  cidr_blocks = var.allowed_ips
}
