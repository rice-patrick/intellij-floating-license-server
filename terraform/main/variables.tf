variable "usage" {
  default = "dev"
  description = "used to tag a bunch of resources. The level of the environment being deployed to"
}

variable "domain_name" {
  default = ""
  description = "Used to issue an SSL certificate via DNS. A Route53 hosted zone must exist with the 'Domain' tag matching this domain"
}

variable "subdomain" {
  default = "www."
  description = "Added to the start of the domain for issuing the SSL certificate"
}

variable "cluster_name" {
  default = "fls-cluster"
}

variable "container_subnets" {
  type = list
  default = ["private-0-az-1","private-0-az-0"]
  description = "The subnets to use when deploying containers"
}

variable "lb_subnets" {
  type = list
  default = ["public-0-az-1","public-0-az-0"]
  description = "The subnets to use when deploying containers"
}

variable "allowed_ips" {
  type = list
  default = ["0.0.0.0/0"]
  description = "What IP addresses are allowed to access the FLS server"
}

variable "lb_visibility" {
  default = "true"
  description = "whether or not the load balancer should be publicly visibile"
}
