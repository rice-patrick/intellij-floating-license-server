variable "usage" {
  default     = "dev"
  description = "The environment level for the resource"
}
